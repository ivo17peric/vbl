package com.wolfshund.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wolfshund.api.model.UserBetsInformation;
import com.wolfshund.utils.Utils;
import com.wolfshund.vbl.R;
import com.wolfshund.view.SimpleAutoFitTextView;

public class UserBetsAdapter extends BaseAdapter {
	
	private List<UserBetsInformation> mDataList;
	private LayoutInflater mInflater;
	private Context mContext;
	
	public UserBetsAdapter (Context context){
		mContext = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setData(List<UserBetsInformation> list){
		mDataList = list;
	}
	
	public void addData(List<UserBetsInformation> list){
		mDataList.addAll(list);
	}

	@Override
	public int getCount() {
		if(mDataList != null) return mDataList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if(mDataList != null) return mDataList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		if(mDataList != null) return mDataList.get(position).getId();
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		
		if(convertView == null){
			holder = new ViewHolder();
			
			convertView = mInflater.inflate(R.layout.item_user_bets, null, false);
			Utils.scaleToFit((Activity) mContext, convertView);
			
			holder.tvBet = (TextView) convertView.findViewById(R.id.tvBetPlaceValue);
			holder.tvCoef = (TextView) convertView.findViewById(R.id.tvCoefValue);
			holder.tvHalfWin = (TextView) convertView.findViewById(R.id.tvHalfWinValue);
			holder.tvWin = (TextView) convertView.findViewById(R.id.tvWinValue);
			holder.tvRound = (TextView) convertView.findViewById(R.id.tvRound);
			holder.viewResult = convertView.findViewById(R.id.viewRes);
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		UserBetsInformation item = mDataList.get(position);
		
		holder.tvBet.setTextSize(30 * Utils.getK());
		holder.tvCoef.setTextSize(30 * Utils.getK());
		holder.tvHalfWin.setTextSize(30 * Utils.getK());
		holder.tvWin.setTextSize(30 * Utils.getK());
		holder.tvRound.setTextSize(30 * Utils.getK());
		
		holder.tvRound.setText("Rnd:" + item.getRound());
		holder.tvBet.setText(String.valueOf(item.getBetPlace()));
		holder.tvCoef.setText(String.valueOf(item.getKoef()));
		
		if(Utils.getBetSize(item.getBet()) > 4){
			double halfWinDouble = item.getBetPlace() * item.getKoef() / item.getMaxKoef(); 
			holder.tvHalfWin.setText(String.format("%.2f", halfWinDouble));
		}else{
			holder.tvHalfWin.setText("-");
		}
		
		holder.tvWin.setText(String.format("%.2f", (item.getBetPlace() * item.getKoef())));
		
		if(item.getResult() == 0){
			holder.viewResult.setBackgroundResource(R.drawable.no_result);
		}else if(item.getResult() == 1){
			holder.viewResult.setBackgroundResource(R.drawable.yes);
		}else if(item.getResult() == 2){
			holder.viewResult.setBackgroundResource(R.drawable.failed);
		}else if(item.getResult() == 3){
			holder.viewResult.setBackgroundResource(R.drawable.half_yes);
		}
		
		if(position % 2 == 0){
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.even_row));
		}else{
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.odd_row));
		}
		
		((SimpleAutoFitTextView)holder.tvBet).callOnMeasure();
		
		return convertView;
	}
	
	class ViewHolder {
		public TextView tvRound;
		public TextView tvBet;
		public TextView tvCoef;
		public TextView tvHalfWin;
		public TextView tvWin;
		public View viewResult;
	}

}
