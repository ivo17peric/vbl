package com.wolfshund.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.wolfshund.api.model.MasterData;
import com.wolfshund.vbl.UsersPagerFragment;

public class UsersPagerAdapter extends FragmentStatePagerAdapter {

	private MasterData data;
	
	public UsersPagerAdapter(FragmentManager fm, MasterData data) {
		super(fm);
		this.data = data;
	}

	@Override
	public Fragment getItem(int arg0) {
		return UsersPagerFragment.create(arg0, data);
	}

	@Override
	public int getCount() {
		return 3;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		super.destroyItem(container, position, object);
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

}
