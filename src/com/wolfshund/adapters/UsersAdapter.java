package com.wolfshund.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wolfshund.api.model.UserInformation;
import com.wolfshund.utils.Utils;
import com.wolfshund.vbl.BaseActivity;
import com.wolfshund.vbl.R;
import com.wolfshund.vbl.UsersFragment;

public class UsersAdapter extends BaseAdapter {
	
	private List<UserInformation> mDataList;
	private LayoutInflater mInflater;
	private Context mContext;
	private int mType;
	
	public UsersAdapter (Context context){
		mContext = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setData(List<UserInformation> list){
		mDataList = list;
	}
	
	public void setType(int type){
		mType = type;
	}

	@Override
	public int getCount() {
		if(mDataList != null) return mDataList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if(mDataList != null) return mDataList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		if(mDataList != null) return mDataList.get(position).getId();
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		
		if(convertView == null){
			holder = new ViewHolder();
			
			convertView = mInflater.inflate(R.layout.item_users, null, false);
			Utils.scaleToFit((Activity) mContext, convertView);
			
			holder.tvName = (TextView) convertView.findViewById(R.id.tvUserName);
			holder.tvPoints = (TextView) convertView.findViewById(R.id.tvUserPoints);
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		UserInformation item = mDataList.get(position);
		
		holder.tvName.setText(item.getUsername());
		
		switch (mType) {
		case UsersFragment.SELECTED_LAST_TEN:
			holder.tvPoints.setText(String.valueOf(item.getPointsTenGames()));
			break;
		case UsersFragment.SELECTED_P_POINTS:
			holder.tvPoints.setText(String.valueOf(item.getpPoints()));
			break;
		case UsersFragment.SELECTED_POINTS:
			holder.tvPoints.setText(String.valueOf(item.getPoints()));
			break;

		default:
			break;
		}
		
		if(item.getUuid().equals(((BaseActivity)mContext).getPreferences().getUuid())){
			holder.tvName.setTextColor(mContext.getResources().getColor(R.color.win_green));
			holder.tvPoints.setTextColor(mContext.getResources().getColor(R.color.win_green));
		}else{
			holder.tvName.setTextColor(Color.WHITE);
			holder.tvPoints.setTextColor(Color.WHITE);
		}
		
		if(position % 2 == 0){
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.even_row));
		}else{
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.odd_row));
		}
		
		return convertView;
	}
	
	class ViewHolder {
		public TextView tvName;
		public TextView tvPoints;
	}

}
