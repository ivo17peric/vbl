package com.wolfshund.dialog;

import com.wolfshund.utils.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

public class BaseDialog extends Dialog {

	public BaseDialog(Context context, int style) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		setOwnerActivity((Activity) context);
	}
	
	@Override
	public void show() {
        try {
        	super.show();
		} catch (Exception e) {
			// View not attached to window manager
			e.printStackTrace();
		}
	}
	
	@Override
	public void dismiss() {
		try {
			super.dismiss();
		} catch (Exception e) {
			// View not attached to window manager
			e.printStackTrace();
		}
	}
	
	protected void setContentView(int layoutResID, int viewToScaleID) {
		super.setContentView(layoutResID);
		scaleToFit(viewToScaleID);
	}

	protected void scaleToFit(int viewToScaleID) {
		Utils.getLayoutHelper().scaleToFit(findViewById(viewToScaleID), getOwnerActivity());
	}

}
