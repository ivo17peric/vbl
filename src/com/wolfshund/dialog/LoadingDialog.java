package com.wolfshund.dialog;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.wolfshund.vbl.R;

public class LoadingDialog extends BaseDialog{
	
	private ImageView mImageLoading;
	private AnimationDrawable mAnimationLoading;
	private TextView mTvDots;
	private Handler hDots = new Handler();
	private Runnable rDots;

	public LoadingDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		
		setContentView(R.layout.dialog_loading, R.id.parent_dialog);
		setCancelable(false);
		
		mImageLoading = (ImageView) findViewById(R.id.image_loading);
		mAnimationLoading = (AnimationDrawable) mImageLoading.getDrawable();
		mTvDots = (TextView) findViewById(R.id.tvLoadingDots);
		
		rDots = new Runnable() {
			
			@Override
			public void run() {
				if(mTvDots.getText().toString().length() == 0){
					mTvDots.setText(".");
				}else if(mTvDots.getText().toString().length() == 1){
					mTvDots.setText("..");
				}else if(mTvDots.getText().toString().length() == 2){
					mTvDots.setText("...");
				}else if(mTvDots.getText().toString().length() == 3){
					mTvDots.setText("");
				}
				hDots.postDelayed(rDots, 200);
			}
		};
		
	}
	
	@Override
	public void show() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			public void run() {

				mAnimationLoading.start();
				hDots.post(rDots);

			}
		}, 10);
		super.show();
	}
	
	@Override
	public void dismiss() {
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				hDots.removeCallbacks(rDots);
				LoadingDialog.super.dismiss();
			}
		}, 300);
	}
	
}
