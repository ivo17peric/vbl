package com.wolfshund.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wolfshund.listeners.DialogListener;
import com.wolfshund.vbl.R;

public class ExportUserDialog extends BaseDialog{
	
	private DialogListener mListener;
	private TextView mTvMessage;
	private TextView mUserId;
	private TextView mPassword;
	private Button mOk;
	
	public ExportUserDialog(Context context, String message, String okString, String userId, String password) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		
		setContentView(R.layout.dialog_export_user, R.id.parent_dialog);
		setCancelable(false);
		
		mTvMessage = (TextView) findViewById(R.id.tvMessage);
		mOk = (Button) findViewById(R.id.btnOk);
		mUserId = (TextView) findViewById(R.id.userIdValue);
		mPassword = (TextView) findViewById(R.id.passwordValue);
		
		mTvMessage.setText(message);
		
		if(!TextUtils.isEmpty(okString)){
			mOk.setText(okString);
		}
		mOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener != null){
					mListener.onOkClicked(ExportUserDialog.this);
				}
			}
		});
		
		mPassword.setText(password);
		mUserId.setText(userId);
		
	}
	
	public void setListener(DialogListener lis){
		mListener = lis;
	}
	
}
