package com.wolfshund.dialog;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.model.BetRoundInformation;
import com.wolfshund.utils.AnimUtils;
import com.wolfshund.utils.Utils;
import com.wolfshund.vbl.R;

public class InfoPlaceBetDialog extends BaseDialog{
	
	public InfoPlaceBetDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		
		setOwnerActivity((Activity) context);
		setContentView(R.layout.dialog_info_place_bet, R.id.parent_dialog);
		setCancelable(true);
		
	}
	
	public void setData(BetRoundInformation item, int pos){
		Utils.populateLastFive(getOwnerActivity(), (LinearLayout) findViewById(R.id.llLastFiveHome), item.getHomeLastFiveForm(), Utils.getK());
		Utils.populateLastFive(getOwnerActivity(), (LinearLayout) findViewById(R.id.llLastFiveAway), item.getAwayLastFiveForm(), Utils.getK());
		
		TextView tvOffDefHome = (TextView) findViewById(R.id.tvHomeOffDef);
		tvOffDefHome.setText(item.getHomeOffence()+"/"+item.getHomeDefence());
		
		TextView tvHome = (TextView) findViewById(R.id.tvHomeTeam);
		tvHome.setText(item.getHomeName());
		
		TextView tvOffDefAway = (TextView) findViewById(R.id.tvAwayOffDef);
		tvOffDefAway.setText(item.getAwayOffence()+"/"+item.getAwayDefence());
		
		TextView tvAway = (TextView) findViewById(R.id.tvAwayTeam);
		tvAway.setText(item.getAwayName());
		
		FrameLayout content = (FrameLayout) findViewById(R.id.content);
		content.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InfoPlaceBetDialog.this.dismiss();
			}
		});
		
		View form = findViewById(R.id.viewHomeForm);
		setForm(form, item.getHomeForm());
		rotateForm(form, (int)(-(item.getHomeForm()-5) * 18));
		
		View formA = findViewById(R.id.viewAwayForm);
		formA.setScaleX(-1f); // flip horizontal
		setForm(formA, item.getAwayForm());
		rotateForm(formA, (int)((item.getAwayForm()-5) * 18));
		
		FrameLayout itemFrame = (FrameLayout) findViewById(R.id.item);
		((LayoutParams)itemFrame.getLayoutParams()).topMargin = (int) (110 * Utils.getK() + (pos * 60 * Utils.getK()));
		
		FrameLayout labels = (FrameLayout) findViewById(R.id.labels);
		((LayoutParams)labels.getLayoutParams()).topMargin = (int) ((110-60) * Utils.getK() + (pos * 60 * Utils.getK()));
		
	}
	
	private void setForm(View view, double form){
		if(form > 6.3){
			view.setBackgroundResource(R.drawable.form_win);
		}else if(form < 3.7){
			view.setBackgroundResource(R.drawable.form_lose);
		}else{
			view.setBackgroundResource(R.drawable.form_stay);
		}
	}
	
	private void rotateForm(View view, int rotate){
		AnimUtils.rotation(view, 0, rotate, 400, 400);
	}
	
	@Override
	public void show() {
		super.show();
		FrameLayout content = (FrameLayout) findViewById(R.id.content);
		AnimUtils.fadeSingleAnim(content, 0, 1, 400);
	}
	
	@Override
	public void dismiss() {
		FrameLayout content = (FrameLayout) findViewById(R.id.content);
		AnimUtils.fadeSingleAnim(content, 1, 0, 400, new AnimatorListenerAdapter() {
			
			@Override
			public void onAnimationEnd(Animator animation) {
				InfoPlaceBetDialog.super.dismiss();
			}
		});
	}
	
}
