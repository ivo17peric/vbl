package com.wolfshund.api;

import android.app.Activity;

import com.wolfshund.api.model.LeagueTableData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetLeagueTable extends Client{
	
	private static String API_URL = Const.ApiUrl.LEAGUE_TABLE;

	public GetLeagueTable(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(){
		runGetClient(API_URL, LeagueTableData.class);
	}
}
