package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.UserBetsData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetUserBets extends Client{
	
	private static String API_URL = Const.ApiUrl.USER_BETS;

	public GetUserBets(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, int page){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.UUID, String.valueOf(uuid)));
		params.add(new BasicNameValuePair(Const.Extra.PAGE, String.valueOf(page)));
		
		runPostClient(API_URL, UserBetsData.class, params);
	}
}
