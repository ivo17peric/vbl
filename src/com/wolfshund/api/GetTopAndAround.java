package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.TopAndAroundData;
import com.wolfshund.api.model.UsersData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetTopAndAround extends Client{
	
	private static String API_URL = Const.ApiUrl.TOP_AND_AROUND;
	private static String API_URL_USERS = Const.ApiUrl.GET_USERS;
	private static String API_URL_USERS_ARROUND = Const.ApiUrl.GET_AROUND_USERS;

	public GetTopAndAround(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, String orderBy){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.UUID, String.valueOf(uuid)));
		params.add(new BasicNameValuePair(Const.TopAndAround.ORDER_BY, String.valueOf(orderBy)));
		
		runPostClient(API_URL, TopAndAroundData.class, params);
	}
	
	public void runClient(String uuid, String orderBy, int page){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.UUID, String.valueOf(uuid)));
		params.add(new BasicNameValuePair(Const.TopAndAround.ORDER_BY, String.valueOf(orderBy)));
		
		runPostClient(API_URL_USERS + "?" + Const.Extra.PAGE+"=" + page, UsersData.class, params);
	}
	
	public void runClient(String uuid, String orderBy, int page, boolean isBelow){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.UUID, String.valueOf(uuid)));
		params.add(new BasicNameValuePair(Const.TopAndAround.ORDER_BY, String.valueOf(orderBy)));
		params.add(new BasicNameValuePair(Const.TopAndAround.TO_BELOW, String.valueOf(isBelow ? 1 : 0)));
		
		runPostClient(API_URL_USERS_ARROUND + "?" + Const.Extra.PAGE+"=" + page, UsersData.class, params);
	}
}
