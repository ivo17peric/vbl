package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.BetRoundData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetBetRound extends Client{
	
	private static String API_URL = Const.ApiUrl.ROUND_FOR_BET;

	public GetBetRound(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(int roundId, int offset, String uuid){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if(roundId != -1) params.add(new BasicNameValuePair(Const.BetRoundModel.ROUND, String.valueOf(roundId)));
		if(offset != -1) params.add(new BasicNameValuePair(Const.BetRoundModel.OFFSET, String.valueOf(offset)));
		params.add(new BasicNameValuePair(Const.MasterDataModel.UUID, uuid));
		
		runPostClient(API_URL, BetRoundData.class, params);
	}
}
