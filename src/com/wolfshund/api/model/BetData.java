package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private List<BetInformation> betInformationList;

	public List<BetInformation> getBetInformationList() {
		return betInformationList;
	}

	public void setBetInformationList(List<BetInformation> betInformationList) {
		this.betInformationList = betInformationList;
	}

}
