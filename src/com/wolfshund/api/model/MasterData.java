package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class MasterData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private MasterInformation masterInformation;

	public MasterInformation getMasterInformation() {
		return masterInformation;
	}

	public void setMasterInformation(MasterInformation masterInformation) {
		this.masterInformation = masterInformation;
	}

}
