package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class TopAndAroundInformation implements Serializable{
	
	private static final long serialVersionUID = -7348128927881304780L;

	@SerializedName(Const.TopAndAround.TOP)
	private List<UserInformation> topList;
	
	@SerializedName(Const.TopAndAround.AROUND)
	private List<UserInformation> aroundList;

	public List<UserInformation> getTopList() {
		return topList;
	}

	public void setTopList(List<UserInformation> topList) {
		this.topList = topList;
	}

	public List<UserInformation> getAroundList() {
		return aroundList;
	}

	public void setAroundList(List<UserInformation> aroundList) {
		this.aroundList = aroundList;
	}
	
}
