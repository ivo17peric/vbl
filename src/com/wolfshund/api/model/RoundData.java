package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class RoundData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private List<RoundInformation> roundInformationList;

	public List<RoundInformation> getRoundInformationList() {
		return roundInformationList;
	}

	public void setRoundInformationList(List<RoundInformation> roundInformationList) {
		this.roundInformationList = roundInformationList;
	}

}
