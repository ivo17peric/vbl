package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class UserBetsInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.UserBetsModel.ID)
	private int id;
	
	@SerializedName(Const.UserBetsModel.BET_PLACE)
	private int betPlace;
	
	@SerializedName(Const.UserBetsModel.SEASON)
	private int season;
	
	@SerializedName(Const.UserBetsModel.ROUND)
	private int round;
	
	@SerializedName(Const.UserBetsModel.USER_ID)
	private int userId;
	
	@SerializedName(Const.UserBetsModel.BET)
	private String bet;
	
	@SerializedName(Const.UserBetsModel.KOEF)
	private double koef;
	
	@SerializedName(Const.UserBetsModel.MAX_KOEF)
	private double maxKoef;
	
	@SerializedName(Const.UserBetsModel.RESULT)
	private int result;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBetPlace() {
		return betPlace;
	}

	public void setBetPlace(int betPlace) {
		this.betPlace = betPlace;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getBet() {
		return bet;
	}

	public void setBet(String bet) {
		this.bet = bet;
	}

	public double getKoef() {
		return koef;
	}

	public void setKoef(double koef) {
		this.koef = koef;
	}

	public double getMaxKoef() {
		return maxKoef;
	}

	public void setMaxKoef(double maxKoef) {
		this.maxKoef = maxKoef;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}
