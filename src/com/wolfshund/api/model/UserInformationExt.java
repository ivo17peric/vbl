package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class UserInformationExt implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.UserDataModel.ID)
	private int id;
	
	@SerializedName(Const.UserDataModel.UUID)
	private String uuid;
	
	@SerializedName(Const.UserDataModel.EMAIL)
	private String email;
	
	@SerializedName(Const.UserDataModel.IS_VIP)
	private int isVip;
	
	@SerializedName(Const.UserDataModel.VIP_TILL)
	private String vipTill;
	
	@SerializedName(Const.UserDataModel.LAST_LOGIN)
	private String lastLogin;
	
	@SerializedName(Const.UserDataModel.DEVICE_ID)
	private String deviceId;
	
	@SerializedName(Const.UserDataModel.FIRST_LOGIN)
	private String firstLogin;
	
	@SerializedName(Const.UserDataModel.P_POINTS)
	private long pPoints;
	
	@SerializedName(Const.UserDataModel.POINTS)
	private long points;
	
	@SerializedName(Const.UserDataModel.POINTS_TEN_GAMES)
	private long pointsTenGames;
	
	@SerializedName(Const.UserDataModel.POINTS_TEN_GAMES_LAST)
	private long pointsTenGamesLast;

	@SerializedName(Const.UserDataModel.USERNAME)
	private String username;
	
	@SerializedName(Const.UserDataModel.POS_MY)
	private int posMy;
	
	@SerializedName(Const.UserDataModel.POS_USER_P_POINTS)
	private int posUserPoints;
	
	@SerializedName(Const.UserDataModel.POS_USER_POINTS)
	private int posAllTimePoints;
	
	@SerializedName(Const.UserDataModel.POS_LAST_TEN_GAMES)
	private int posLastTenGamesPoints;
	
	@SerializedName(Const.UserDataModel.POST_TEN_GAMES)
	private int posTenGamesPoints;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIsVip() {
		return isVip;
	}

	public void setIsVip(int isVip) {
		this.isVip = isVip;
	}

	public String getVipTill() {
		return vipTill;
	}

	public void setVipTill(String vipTill) {
		this.vipTill = vipTill;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	public long getpPoints() {
		return pPoints;
	}

	public void setpPoints(long pPoints) {
		this.pPoints = pPoints;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public long getPointsTenGames() {
		return pointsTenGames;
	}

	public void setPointsTenGames(long pointsTenGames) {
		this.pointsTenGames = pointsTenGames;
	}

	public long getPointsTenGamesLast() {
		return pointsTenGamesLast;
	}

	public void setPointsTenGamesLast(long pointsTenGamesLast) {
		this.pointsTenGamesLast = pointsTenGamesLast;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getPosMy() {
		return posMy;
	}

	public void setPosMy(int posMy) {
		this.posMy = posMy;
	}

	public int getPosUserPoints() {
		return posUserPoints;
	}

	public void setPosUserPoints(int posUserPoints) {
		this.posUserPoints = posUserPoints;
	}

	public int getPosAllTimePoints() {
		return posAllTimePoints;
	}

	public void setPosAllTimePoints(int posAllTimePoints) {
		this.posAllTimePoints = posAllTimePoints;
	}

	public int getPosLastTenGamesPoints() {
		return posLastTenGamesPoints;
	}

	public void setPosLastTenGamesPoints(int posLastTenGamesPoints) {
		this.posLastTenGamesPoints = posLastTenGamesPoints;
	}

	public int getPosTenGamesPoints() {
		return posTenGamesPoints;
	}

	public void setPosTenGamesPoints(int posTenGamesPoints) {
		this.posTenGamesPoints = posTenGamesPoints;
	}
	
}
