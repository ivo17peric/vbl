package com.wolfshund.api.model;


public class TagBetModel {
	
	private int matchId;
	private double coef;
	private int pos;
	private int posBetType;
	private int selectedPosBetType;
	private int betType;
	
	public TagBetModel(int matchId, double coef, int pos, int posBetType, int selectedBetType, int betType) {
		super();
		this.matchId = matchId;
		this.coef = coef;
		this.pos = pos;
		this.posBetType = posBetType;
		this.selectedPosBetType = selectedBetType;
		this.betType = betType;
	}
	
	public int getMatchId() {
		return matchId;
	}
	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}
	public double getCoef() {
		return coef;
	}
	public void setCoef(double coef) {
		this.coef = coef;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}

	public int getPosBetType() {
		return posBetType;
	}

	public void setPosBetType(int posBetType) {
		this.posBetType = posBetType;
	}

	public int getSelectedPosBetType() {
		return selectedPosBetType;
	}

	public void setSelectedPosBetType(int selectedBetType) {
		this.selectedPosBetType = selectedBetType;
	}

	public int getBetType() {
		return betType;
	}

	public void setBetType(int betType) {
		this.betType = betType;
	}
	
}
