package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class UsersData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private List<UserInformation> users;

	public List<UserInformation> getUsers() {
		return users;
	}

	public void setUsers(List<UserInformation> users) {
		this.users = users;
	}
}
