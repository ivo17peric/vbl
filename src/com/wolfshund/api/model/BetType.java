package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetType implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.BetRoundModel.HOME_WIN)
	private double homeWin;
	
	@SerializedName(Const.BetRoundModel.DRAW)
	private double draw;
	
	@SerializedName(Const.BetRoundModel.AWAY_WIN)
	private double awayWin;
	
	@SerializedName(Const.BetRoundModel.HOME_DRAW_WIN)
	private double homeDrawWin;
	
	@SerializedName(Const.BetRoundModel.AWAY_DRAW_WIN)
	private double awayDrawWin;
	
	@SerializedName(Const.BetRoundModel.HOME_HENDICAP)
	private double homeHendicap;
	
	@SerializedName(Const.BetRoundModel.AWAY_HENDICAP)
	private double awayHendicap;

	public double getHomeWin() {
		return homeWin;
	}

	public void setHomeWin(double homeWin) {
		this.homeWin = homeWin;
	}

	public double getDraw() {
		return draw;
	}

	public void setDraw(double draw) {
		this.draw = draw;
	}

	public double getAwayWin() {
		return awayWin;
	}

	public void setAwayWin(double awayWin) {
		this.awayWin = awayWin;
	}

	public double getHomeDrawWin() {
		return homeDrawWin;
	}

	public void setHomeDrawWin(double homeDrawWin) {
		this.homeDrawWin = homeDrawWin;
	}

	public double getAwayDrawWin() {
		return awayDrawWin;
	}

	public void setAwayDrawWin(double awayDrawWin) {
		this.awayDrawWin = awayDrawWin;
	}

	public double getHomeHendicap() {
		return homeHendicap;
	}

	public void setHomeHendicap(double homeHendicap) {
		this.homeHendicap = homeHendicap;
	}

	public double getAwayHendicap() {
		return awayHendicap;
	}

	public void setAwayHendicap(double awayHendicap) {
		this.awayHendicap = awayHendicap;
	}
	
}
