package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BaseModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.BaseModel.RESULT)
	private int result;
	
	@SerializedName(Const.BaseModel.MESSAGE)
	private String message;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
