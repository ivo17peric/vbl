package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class TopAndAroundData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private TopAndAroundInformation topAndAround;

	public TopAndAroundInformation getTopAndAround() {
		return topAndAround;
	}

	public void setTopAndAround(TopAndAroundInformation topAndAround) {
		this.topAndAround = topAndAround;
	}
	
}
