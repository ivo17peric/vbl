package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class UserData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private UserInformation userData;
	
	public UserInformation getUserData() {
		return userData;
	}

	public void setUserData(UserInformation userData) {
		this.userData = userData;
	}

}
