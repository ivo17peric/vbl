package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class UserBetsData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private UserBetsAroundData aroundData;

	public UserBetsAroundData getAroundData() {
		return aroundData;
	}

	public void setmAroundData(UserBetsAroundData aroundData) {
		this.aroundData = aroundData;
	}

}
