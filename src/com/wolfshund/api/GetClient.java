package com.wolfshund.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.wolfshund.utils.Const;
import com.wolfshund.utils.LogCS;

public class GetClient extends BaseClient{

	private InputStream is = null;
	private JSONObject jObj = null;
	
	public JSONObject getJsonObject(String url){
		LogCS.d("URL", url);
		// defaultHttpClient
		HttpParams httpParams = new BasicHttpParams();
		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(httpParams, "UTF-8");
		// Set the timeout in milliseconds until a connection is
		// established.
		int timeoutConnection = 60000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 60000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
		
		HttpClient httpClient = new DefaultHttpClient(httpParams);
		
		HttpGet request = new HttpGet(url.toString());
		
		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(request);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String json;
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, Charset.forName("UTF-8")), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			json = sb.toString();

		} catch (Exception e) {
			LogCS.e("Buffer Error", "Error converting result " + e.toString());
			json = "";
		}
        LogCS.d("JSON", json);
        
		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			LogCS.e("JSON Parser", "Error parsing data " + e.toString());
			jObj = null;
		}
		
		// return JSON String
		try {
			return jObj == null ? new JSONObject().put(Const.Error.ERROR_MESSAGE, Const.Error.DEFAULT_MESSAGE) : jObj;
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	protected JSONObject getJsonObject(String url, List<NameValuePair> params) {
		return null;
	}
	
}
