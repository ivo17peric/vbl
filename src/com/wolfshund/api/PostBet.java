package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.ApiResponse;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class PostBet extends Client{
	
	private static String API_URL = Const.ApiUrl.SEND_BET;

	public PostBet(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, String bets, String round, String coef, String maxCoef, String placeBet){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.PostBet.UUID, uuid));
		params.add(new BasicNameValuePair(Const.PostBet.BET, bets));
		params.add(new BasicNameValuePair(Const.PostBet.ROUND, round));
		params.add(new BasicNameValuePair(Const.PostBet.COEF, coef));
		params.add(new BasicNameValuePair(Const.PostBet.MAX_COEF, maxCoef));
		params.add(new BasicNameValuePair(Const.PostBet.PLACE_BET, placeBet));
		
		runPostClient(API_URL, ApiResponse.class, params);
	}
}
