package com.wolfshund.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.provider.Settings;

public class GetUserDetails {

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);

        Account account = getAccount(accountManager);

        if (account == null) {
            return "no mail";
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String generateUUID(){
        return Utils.generateUUID(System.currentTimeMillis());
    }
}
