package com.wolfshund.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;


public class Preferences {

	private static final String PREFERENCES_UUID = "user_specific_uuid";
	private static final String PREFERENCES_EMAIL = "user_specific_email";
	private static final String PREFERENCES_USERNAME = "user_specific_username";
	private static final String PREFERENCES_DEVICE_ID = "user_specific_device_id";
	
	private SharedPreferences mSharedPreferences;

	public Preferences(Context ctx) {
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
	}

	public void setUUID(String uuid){
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(PREFERENCES_UUID, uuid);
		editor.commit();
	}
	
	public String getUuid() {
		return mSharedPreferences.getString(PREFERENCES_UUID, "");
	}
	
	public void setEmail(String email){
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(PREFERENCES_EMAIL, email);
		editor.commit();
	}
	
	public String getEmail() {
		return mSharedPreferences.getString(PREFERENCES_EMAIL, "");
	}
	
	public void setUsername(String username){
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(PREFERENCES_USERNAME, username);
		editor.commit();
	}
	
	public String getUsername() {
		return mSharedPreferences.getString(PREFERENCES_USERNAME, "");
	}
	
	public void setDeviceId(String deviceId){
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(PREFERENCES_DEVICE_ID, deviceId);
		editor.commit();
	}
	
	public String getDeviceId() {
		return mSharedPreferences.getString(PREFERENCES_DEVICE_ID, "");
	}
	
	public void setCustomBoolean(String key, boolean value){
		if(TextUtils.isEmpty(key)) return;
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	public boolean getCustomBoolean(String key){
		return mSharedPreferences.getBoolean(key, false);
	}
}