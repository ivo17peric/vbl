package com.wolfshund.listeners;

public interface ErrorApiListener {
	public void onCancel();
    public void onRetry();
}
