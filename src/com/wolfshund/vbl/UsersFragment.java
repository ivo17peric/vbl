package com.wolfshund.vbl;

import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.wolfshund.adapters.UsersPagerAdapter;
import com.wolfshund.api.model.MasterData;
import com.wolfshund.utils.Const;
import com.wolfshund.view.ViewPagerWithDisabling;

public class UsersFragment extends BaseFragment{
	
	private static final int LEFT = 0;
	private static final int RIGHT = 2;
	
	public static final int SELECTED_P_POINTS = 0;
	public static final int SELECTED_LAST_TEN = 1;
	public static final int SELECTED_POINTS = 2;
	
	private MasterData mData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_users, R.id.parentUsersFragment, inflater, container);
		
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof HomeActivity) {
					((HomeActivity) getActivity()).startNextActivity(Const.Home.USERS);
				}
			}
		});
		
		mData = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
		
		final ViewPagerWithDisabling pager = (ViewPagerWithDisabling) view.findViewById(R.id.usersPager);
		pager.setPagingEnabled(true);
		UsersPagerAdapter adapter = new UsersPagerAdapter(getFragmentManager(), mData);
		pager.setAdapter(adapter);
		pager.setCurrentItem(1);
		
		final View left = view.findViewById(R.id.viewPagerLeft);
		final View right = view.findViewById(R.id.viewPagerRight);
		
		right.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(pager.getCurrentItem()+1 <= 2){
					pager.setCurrentItem(pager.getCurrentItem()+1);
				}
			}
		});
		
		left.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(pager.getCurrentItem()-1 >= 0){
					pager.setCurrentItem(pager.getCurrentItem()-1);
				}
			}
		});
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				manageArrows(arg0, left, right);
				manageHome(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		
		return view;
	}
	
	private void manageArrows(int type, View left, View right){
		if(type == LEFT){
			left.setVisibility(View.INVISIBLE);
		}else if(type == RIGHT){
			right.setVisibility(View.INVISIBLE);
		}else{
			left.setVisibility(View.VISIBLE);
			right.setVisibility(View.VISIBLE);
		}
	}
	
	private void manageHome(int pos){
		if(getActivity() instanceof HomeActivity){
			switch (pos) {
			case 0:
				((HomeActivity)getActivity()).setSelectedUsers(SELECTED_P_POINTS);
				break;
			case 1:
				((HomeActivity)getActivity()).setSelectedUsers(SELECTED_LAST_TEN);
				break;
			case 2:
				((HomeActivity)getActivity()).setSelectedUsers(SELECTED_POINTS);
				break;

			default:
				break;
			}
			
		}
	}
	
}
