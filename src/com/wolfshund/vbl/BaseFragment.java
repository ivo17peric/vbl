package com.wolfshund.vbl;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wolfshund.utils.Utils;

public class BaseFragment extends Fragment {
	
	private boolean isActive=false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		isActive=true;

		Utils.initializeK(getActivity());
		
	}
	
	protected View setView(int layoutResID, int viewToScaleID, LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(layoutResID, container, false);
		scaleToFit(v.findViewById(viewToScaleID));
		return v;
	}
	
	protected void scaleToFit(View viewToScaleID) {
		Utils.getLayoutHelper().scaleToFit(viewToScaleID, getActivity());
	}
	
	protected boolean isActive() {
		return isActive;
	}

}
