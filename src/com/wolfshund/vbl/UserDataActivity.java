package com.wolfshund.vbl;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;

import com.wolfshund.api.ExportUser;
import com.wolfshund.api.GetUserData;
import com.wolfshund.api.ImportUser;
import com.wolfshund.api.model.BaseModel;
import com.wolfshund.api.model.UserDataExt;
import com.wolfshund.dialog.AppDialog;
import com.wolfshund.dialog.ExportUserDialog;
import com.wolfshund.dialog.ImportUserDialog;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.DialogListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.GetUserDetails;
import com.wolfshund.utils.Utils;
import com.wolfshund.view.SimpleAutoFitTextView;

public class UserDataActivity extends BaseActivity implements OnClickListener{
	
	private int userId;

	public static void start(Context c) {
		Intent intent = new Intent(c, UserDataActivity.class);
		startActivityWithAnimation(c, intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_data, R.id.parentUserData);
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.USERDATA, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getData();
					}
				});
			}
		}, 300);
		
		findViewById(R.id.exportUser).setOnClickListener(this);
		findViewById(R.id.importUser).setOnClickListener(this);
	}
	
	private void getData() {
		final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetUserData client = new GetUserData(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				setData((UserDataExt)result);
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finish();
			}
		});
    	
    	client.runClientExt(getPreferences().getUuid());
	}

	protected void setData(UserDataExt result) {
		SimpleAutoFitTextView userName = (SimpleAutoFitTextView) findViewById(R.id.usernameValue);
		SimpleAutoFitTextView email = (SimpleAutoFitTextView) findViewById(R.id.emailValue);
		SimpleAutoFitTextView firstLogin = (SimpleAutoFitTextView) findViewById(R.id.firstLoginValue);
		SimpleAutoFitTextView lastLogin = (SimpleAutoFitTextView) findViewById(R.id.lastLoginValue);
		
		userName.setText(result.getUserData().getUsername());
		email.setText(result.getUserData().getEmail());
		firstLogin.setText(result.getUserData().getFirstLogin());
		lastLogin.setText(result.getUserData().getLastLogin());
		userName.callOnMeasure();
		email.callOnMeasure();
		firstLogin.callOnMeasure();
		lastLogin.callOnMeasure();
		
		SimpleAutoFitTextView userPoints = (SimpleAutoFitTextView) findViewById(R.id.userPointValue);
		SimpleAutoFitTextView allTimePoints = (SimpleAutoFitTextView) findViewById(R.id.allTimePointValue);
		SimpleAutoFitTextView tenGamesPoints = (SimpleAutoFitTextView) findViewById(R.id.tenGamesPointValue);
		SimpleAutoFitTextView lastTenGamesPoints = (SimpleAutoFitTextView) findViewById(R.id.lastTenGamesPointValue);
		
		userPoints.setText(String.valueOf(result.getUserData().getpPoints()));
		allTimePoints.setText(String.valueOf(result.getUserData().getPoints()));
		tenGamesPoints.setText(String.valueOf(result.getUserData().getPointsTenGames()));
		lastTenGamesPoints.setText(String.valueOf(result.getUserData().getPointsTenGamesLast()));
		userPoints.callOnMeasure();
		allTimePoints.callOnMeasure();
		tenGamesPoints.callOnMeasure();
		lastTenGamesPoints.callOnMeasure();
		
		SimpleAutoFitTextView userPointsPos = (SimpleAutoFitTextView) findViewById(R.id.userPointPosition);
		SimpleAutoFitTextView allTimePointsPos = (SimpleAutoFitTextView) findViewById(R.id.allTimePointPosition);
		SimpleAutoFitTextView tenGamesPointsPos = (SimpleAutoFitTextView) findViewById(R.id.tenGamesPointPosition);
		SimpleAutoFitTextView lastTenGamesPointsPos = (SimpleAutoFitTextView) findViewById(R.id.lastTenGamesPointPosition);
		
		userPointsPos.setText(result.getUserData().getPosUserPoints() + ".");
		allTimePointsPos.setText(result.getUserData().getPosAllTimePoints() + ".");
		tenGamesPointsPos.setText(result.getUserData().getPosTenGamesPoints() + ".");
		lastTenGamesPointsPos.setText(result.getUserData().getPosLastTenGamesPoints() + ".");
		userPointsPos.callOnMeasure();
		allTimePointsPos.callOnMeasure();
		tenGamesPointsPos.callOnMeasure();
		lastTenGamesPointsPos.callOnMeasure();
		
		userId = result.getUserData().getId();
	}

	@Override
	public void onBackPressed() {
		finishWithanimation(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.exportUser:
			exportUser();
			break;
		case R.id.importUser:
			importUser();
			break;
		default:
			break;
		}
	}

	private void exportUser() {
		final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	final String password = Utils.getRadnomNumber(10);
    	ExportUser client = new ExportUser(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				String mess = ((BaseModel)result).getMessage() + "\n" + getString(R.string.you_can_use_this_data_for_import_user_on_other_device_);
				ExportUserDialog dialogApp = new ExportUserDialog(UserDataActivity.this, mess, getString(R.string.ok), String.valueOf(userId), password);
				dialogApp.setListener(new DialogListener() {
					
					@Override
					public void onOkClicked(Dialog d) {
						d.dismiss();
					}
					
					@Override
					public void onCancelClicked(Dialog d) {}
				});
				dialogApp.show();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();				
			}
			
			@Override
			public void onCancel() {
				
			}
		});
    	
    	client.runClient(getPreferences().getUuid(), password);
	}

	private void importUser() {
		AppDialog appDailog = new AppDialog(this, getString(R.string.if_you_import_user_current_user_will_be_deleted_permanently_), 
				getString(R.string.ok), getString(R.string.cancel));
		appDailog.setListener(new DialogListener() {
			
			@Override
			public void onOkClicked(Dialog d) {
				importUserExecute();
				d.dismiss();
			}
			
			@Override
			public void onCancelClicked(Dialog d) {
				d.dismiss();
			}
		});
		
		appDailog.show();
	}

	protected void importUserExecute() {
		ImportUserDialog dialogImport = new ImportUserDialog(this, getString(R.string.fill_data_), getString(R.string.ok), getString(R.string.cancel_capital));
		dialogImport.setListener(new DialogListener() {
			
			@Override
			public void onOkClicked(Dialog d) {
				String password = ((ImportUserDialog)d).getPassword();
				String userId = ((ImportUserDialog)d).getUserId();
				importUserExecClient(password, userId);
			}
			
			@Override
			public void onCancelClicked(Dialog d) {
				d.dismiss();
			}
		});
		
		dialogImport.show();
	}

	protected void importUserExecClient(String password, String userId2) {
		final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	ImportUser client = new ImportUser(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				String mess = ((BaseModel)result).getMessage() + "\n" + getString(R.string.reset_aplication_for_new_data_);
				AppDialog dialogApp = new AppDialog(UserDataActivity.this, mess, getString(R.string.ok), null);
				dialogApp.setListener(new DialogListener() {
					
					@Override
					public void onOkClicked(Dialog d) {
						d.dismiss();
						finish();
						moveTaskToBack(true);
						System.exit(0);
					}
					
					@Override
					public void onCancelClicked(Dialog d) {}
				});
				dialogApp.show();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();				
			}
			
			@Override
			public void onCancel() {
				
			}
		});
    	
    	client.runClient(getPreferences().getUuid(), password, userId2, GetUserDetails.getDeviceId(this));
	}

}
