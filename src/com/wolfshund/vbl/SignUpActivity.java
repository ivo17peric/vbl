package com.wolfshund.vbl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wolfshund.api.PostUser;
import com.wolfshund.dialog.AppDialog;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.GetUserDetails;

public class SignUpActivity extends BaseActivity {
	
	public static void start(Context c){
		Intent intent = new Intent(c, SignUpActivity.class);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up, R.id.parentSign);
        
        final String uuid = GetUserDetails.generateUUID();
        final String email = GetUserDetails.getEmail(this);
        final String deviceId = GetUserDetails.getDeviceId(this);
        
        final EditText username = (EditText) findViewById(R.id.etUsername);
        
		InputFilter filter = new InputFilter() {
			
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				for (int i = start; i < end; i++) {
					if (!Character.isLetterOrDigit(source.charAt(i)) && source.charAt(i) != '-' && source.charAt(i) != '_' && source.charAt(i) != '.') {
						return "";
					}
				}
				return null;
			}
		};

		username.setFilters(new InputFilter[] { filter });
		
		Button btn = (Button) findViewById(R.id.btnOk);
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String usernameS = username.getText().toString();
				if(checkValidUsername(usernameS)){
					sendData(uuid, email, usernameS, deviceId);
				}
			}
		});
    }
	
	private boolean checkValidUsername(String name){
		if(TextUtils.isEmpty(name)){
			AppDialog dialog = new AppDialog(this, getString(R.string.please_enter_your_name), getString(R.string.close), null);
			dialog.show();
			return false;
		}else{
			return true;
		}
	}
	
	private void sendData(final String uuid, final String email, final String username, final String deviceId){
		final LoadingDialog lDialog = new LoadingDialog(this);
		lDialog.show();
		PostUser client = new PostUser(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				lDialog.dismiss();
				getPreferences().setUUID(uuid);
				getPreferences().setEmail(email);
				getPreferences().setUsername(username);
				getPreferences().setDeviceId(deviceId);
				
				HomeActivity.start(SignUpActivity.this);
				finish();
			}
			
			@Override
			public void onError(String errorMessage) {
				lDialog.dismiss();
			}
		});
		
		client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				lDialog.show();
			}
			
			@Override
			public void onCancel() {
				finish();
			}
		});
		
		client.runClient(uuid, email, username, deviceId);
	}
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
}
