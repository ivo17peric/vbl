package com.wolfshund.vbl;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.GetLeagueTable;
import com.wolfshund.api.model.LeagueTableData;
import com.wolfshund.api.model.LeagueTableInformation;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class TableActivity extends BaseActivity {
	
	private List<LeagueTableInformation> mData;
	
	public static void start(Context c){
		Intent intent = new Intent(c, TableActivity.class);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table, R.id.parentTable);
        
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.TABLE, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getTable();
					}
				});
			}
		}, 300);
    }
    
    private void setData(){
    	LinearLayout layout = (LinearLayout) findViewById(R.id.llInScrollViewTableDetailed);
		
		int i = 1;
		for(LeagueTableInformation item : mData){
			View view = LayoutInflater.from(this).inflate(R.layout.item_table_detailed, layout, false);
			Utils.scaleToFit(this, view);
			
			TextView tvPosition = (TextView) view.findViewById(R.id.tvPosition);
			tvPosition.setText(i+".");
			
			TextView tvName = (TextView) view.findViewById(R.id.tvClubName);
			tvName.setText(item.getName());
			
			TextView tvGamePlayed = (TextView) view.findViewById(R.id.tvGamePlayed);
			tvGamePlayed.setText(String.valueOf(item.getGames()));
			
			TextView tvWinGames = (TextView) view.findViewById(R.id.tvWinGames);
			tvWinGames.setText(String.valueOf(item.getWinGames()));
			
			TextView tvDrawGames = (TextView) view.findViewById(R.id.tvDrawGames);
			tvDrawGames.setText(String.valueOf(item.getDrawGames()));
			
			TextView tvLoseGames = (TextView) view.findViewById(R.id.tvLoseGames);
			tvLoseGames.setText(String.valueOf(item.getLoseGames()));
			
			TextView tvGoalScored = (TextView) view.findViewById(R.id.tvGoalScored);
			tvGoalScored.setText(String.valueOf(item.getGoalsScored()));
			
			TextView tvGoalAllowed = (TextView) view.findViewById(R.id.tvGoalAllowed);
			tvGoalAllowed.setText(String.valueOf(item.getGoalsAllowed()));
			
			TextView tvGoalDif = (TextView) view.findViewById(R.id.tvGoalDifference);
			tvGoalDif.setText(String.valueOf((item.getGoalsScored() - item.getGoalsAllowed())));
			
			TextView tvPoints = (TextView) view.findViewById(R.id.tvPoints);
			tvPoints.setText(String.valueOf(item.getPoints()));
			
			TextView tvOffence = (TextView) view.findViewById(R.id.tvOffence);
			tvOffence.setText(String.valueOf(item.getOffence()));
			
			TextView tvDefence = (TextView) view.findViewById(R.id.tvDefence);
			tvDefence.setText(String.valueOf(item.getDefence()));
			
			TextView tvFrom = (TextView) view.findViewById(R.id.tvForm);
			tvFrom.setText(String.valueOf(item.getForm()));
			
			Utils.populateLastFive(this, (LinearLayout) view.findViewById(R.id.llLastFive), item.getLastFiveForm(), k);
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			layout.addView(view);
		}
    }
    
    private void getTable(){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetLeagueTable client = new GetLeagueTable(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mData = ((LeagueTableData)result).getLeagueTableData();
				setData();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finishWithanimation(TableActivity.this);
			}
		});
    	
    	client.runClient();
    }
    
    @Override
    public void onBackPressed() {
    	finishWithanimation(this);
    }
    
}
