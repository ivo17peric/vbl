package com.wolfshund.vbl;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.wolfshund.adapters.UsersAdapter;
import com.wolfshund.api.model.MasterData;
import com.wolfshund.api.model.UserInformation;
import com.wolfshund.utils.Const;

public class UsersPagerFragment extends BaseFragment{
	
	public static final int NO_RESULT = 99;
	
	private int mPageNumber;
	private List<UserInformation> mUsersList;
	
	public static UsersPagerFragment create(int pageNumber, MasterData data) {
		UsersPagerFragment fragment = new UsersPagerFragment();
		Bundle args = new Bundle();
		args.putInt(Const.Extra.PAGE, pageNumber);
		args.putSerializable(Const.Extra.MASTER, data);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPageNumber = getArguments().getInt(Const.Extra.PAGE);
		MasterData data = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
		if (mPageNumber == 0) mUsersList = data.getMasterInformation().getUsersPPoints();
		if (mPageNumber == 1) mUsersList = data.getMasterInformation().getUsersPointsTenGames();
		if (mPageNumber == 2) mUsersList = data.getMasterInformation().getUsersPoints();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_users_pager, R.id.parentUsersPagerFragment, inflater, container);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ListView lvUsers = (ListView) getView().findViewById(R.id.lvUsers);
		lvUsers.setAdapter(new UsersAdapter(getActivity()));
	    ((UsersAdapter) lvUsers.getAdapter()).setData(mUsersList);
	    if (mPageNumber == 0) ((UsersAdapter) lvUsers.getAdapter()).setType(UsersFragment.SELECTED_P_POINTS);
		if (mPageNumber == 1) ((UsersAdapter) lvUsers.getAdapter()).setType(UsersFragment.SELECTED_LAST_TEN);
		if (mPageNumber == 2) ((UsersAdapter) lvUsers.getAdapter()).setType(UsersFragment.SELECTED_POINTS);
		
		lvUsers.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(getActivity() instanceof HomeActivity){
					((HomeActivity)getActivity()).startNextActivity(Const.Home.USERS);
				}
			}
		});
		
		TextView title = (TextView) getView().findViewById(R.id.tvTitle);
		if (mPageNumber == 0) title.setText("Users points");
		if (mPageNumber == 1) title.setText("Ten games points");
		if (mPageNumber == 2) title.setText("All time points");
	}
	
	public int getPageNumber() {
		return mPageNumber;
	}
	
}
