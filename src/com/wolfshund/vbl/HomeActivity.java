package com.wolfshund.vbl;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.FrameLayout;

import com.wolfshund.api.GetMasterData;
import com.wolfshund.api.model.MasterData;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;

public class HomeActivity extends BaseActivity {
	
	private int mType = Const.Home.NONE;
	private int mSelectedRoundInPager = 1;
	private int mSelectedUsers = UsersFragment.SELECTED_LAST_TEN;
	
	private MasterData mMaster;
	private boolean toRefresh = false;
	
	public static void start(Context c){
		Intent intent = new Intent(c, HomeActivity.class);
		startActivityWithAnimation(c, intent);
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home, R.id.parentHome);
        
        checkForTutorial(Const.Activites.HOME, new CheckTutorialListener() {

			@Override
			public void onTutorialCheckFinish() {
				getMasterData();
			}
		});
        
        LocalBroadcastManager.getInstance(this).registerReceiver(refreshDataReceiver, new IntentFilter(Const.REFRESH_DATA_INTENT));
        
    }
    
    @Override
    protected void onDestroy() {
    	LocalBroadcastManager.getInstance(this).unregisterReceiver(refreshDataReceiver);
    	super.onDestroy();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	mType = Const.Home.NONE;
    	
    	if(toRefresh){
    		getMasterData();
    		toRefresh = false;
    	}
    }
    
    private BroadcastReceiver refreshDataReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			toRefresh = true;
		}
	};
    
    private void getMasterData(){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetMasterData client = new GetMasterData(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mMaster = (MasterData) result;
				setFragments();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				
			}
		});
    	
    	client.runClient(getPreferences().getUuid());
    }
    
    private void setFragments(){
    	FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.Extra.MASTER, mMaster);
        
        FrameLayout roundLayout = (FrameLayout) findViewById(R.id.roundLayout);
        roundLayout.removeAllViews();
        RoundFragment roundFragment = new RoundFragment();
        roundFragment.setArguments(bundle);
        fragmentTransaction.add(roundLayout.getId(), roundFragment, "fragment_" + roundFragment.getClass().getSimpleName());
        
        FrameLayout betsLayout = (FrameLayout) findViewById(R.id.betsLayout);
        betsLayout.removeAllViews();
        BetsFragment betsFragment = new BetsFragment();
        betsFragment.setArguments(bundle);
        fragmentTransaction.add(betsLayout.getId(), betsFragment, "fragment_" + betsFragment.getClass().getSimpleName());
        
        FrameLayout tableLayout = (FrameLayout) findViewById(R.id.tableLayout);
        tableLayout.removeAllViews();
        TableFragment tableFragment = new TableFragment();
        tableFragment.setArguments(bundle);
        fragmentTransaction.add(tableLayout.getId(), tableFragment, "fragment_" + tableFragment.getClass().getSimpleName());
        
        FrameLayout userDataLayout = (FrameLayout) findViewById(R.id.userDataLayout);
        userDataLayout.removeAllViews();
        UserDataFragment userDataFragment = new UserDataFragment();
        userDataFragment.setArguments(bundle);
        fragmentTransaction.add(userDataLayout.getId(), userDataFragment, "fragment_" + userDataFragment.getClass().getSimpleName());
        
        FrameLayout usersLayout = (FrameLayout) findViewById(R.id.usersLayout);
        usersLayout.removeAllViews();
        UsersFragment usersFragment = new UsersFragment();
        usersFragment.setArguments(bundle);
        fragmentTransaction.add(usersLayout.getId(), usersFragment, "fragment_" + usersFragment.getClass().getSimpleName());
        
        fragmentTransaction.commit();
    }
    
    public void startNextActivity(final int type){
    	if(mType != Const.Home.NONE) return;
    	mType = type;
    	
    	start();
    	
    }
    
    private void start(){
    	switch (mType) {
		case Const.Home.ROUND:
			RoundActivity.start(this, mSelectedRoundInPager);
			break;
		case Const.Home.USERS:
			UsersActivity.start(this, mSelectedUsers);
			break;
		case Const.Home.BETS:
			BetsActivity.start(this);
			break;
		case Const.Home.TABLE:
			TableActivity.start(this);
			break;
		case Const.Home.USER_DATA:
			UserDataActivity.start(this);
			break;

		default:
			break;
		}
    }
    
    public void setSelectedRound(int selectedRound){
    	mSelectedRoundInPager = selectedRound;
    }
    
    public void setSelectedUsers(int selectedUsers){
    	mSelectedUsers = selectedUsers;
    }
    
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    }
    
    public class DiplomskiRad{
    	
    	private String content;
    	
    	public void addSection(String secondSection){
    		content = addToContent(secondSection);
    	}
    	
    	public String getContent(){
    		return content;
    	}
    	
    	private String addToContent(String newContent){
    		StringBuilder sb = new StringBuilder();
    		sb.append(content);
    		sb.append(newContent);
    		return sb.toString();
    	}
    	
    }
}

